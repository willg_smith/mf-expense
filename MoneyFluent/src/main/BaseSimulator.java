package main;

import main.LaunchInterface;

public class BaseSimulator {
	
	public static void main(String[] args) {
		LaunchInterface.execute(args);
	}

}
